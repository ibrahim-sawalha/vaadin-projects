package ics.cs.support.model.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import ics.cs.support.model.entity.Product;

public class ProductMapper implements RowMapper<Product> {
    public Product mapRow(ResultSet rs, int rowNum) throws SQLException {
        Product product = new Product();
        product.setId(rs.getBigDecimal("PROD_ID"));
        product.setName(rs.getString("PROD_NAME_REF"));
        return product;
    }
}
