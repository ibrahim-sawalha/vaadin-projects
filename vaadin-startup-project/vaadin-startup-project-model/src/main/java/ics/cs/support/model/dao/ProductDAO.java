package ics.cs.support.model.dao;

import java.util.List;

import ics.cs.support.model.entity.Product;

public interface ProductDAO {
    public List<Product> getAllProducts();
}
