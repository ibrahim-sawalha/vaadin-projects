package ics.cs.support.model.service;

import java.util.List;

import ics.cs.support.model.dao.ProductDAO;
import ics.cs.support.model.entity.Product;
import ics.cs.support.model.mapper.ProductMapper;

// @Repository
public class ProductDAOImpl extends AbstractService implements ProductDAO {

    @Override
    public List<Product> getAllProducts() {
        String SQL = "SELECT prod_id, prod_name_ref from PS_TDPRD";
        try {
            return getJdbcTemplate().query(SQL, new ProductMapper());
        } catch (org.springframework.dao.EmptyResultDataAccessException e) {
        }
        return null;

    }

}
