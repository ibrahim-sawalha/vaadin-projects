package ics.cs.support.config.security;

import javax.servlet.annotation.WebListener;
import javax.servlet.annotation.WebServlet;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.method.configuration.GlobalMethodSecurityConfiguration;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.request.RequestContextListener;

import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.spring.annotation.EnableVaadin;
import com.vaadin.spring.annotation.EnableVaadinNavigation;
import com.vaadin.spring.server.SpringVaadinServlet;

import ics.cs.support.model.dao.ProductDAO;
import ics.cs.support.model.service.ProductDAOImpl;
import ics.cs.support.ui.SecuredUI;

@Configuration
@EnableVaadin
@EnableVaadinNavigation
@EnableGlobalMethodSecurity (securedEnabled = true)
@ImportResource ("/WEB-INF/database-config.xml")
@ComponentScan (basePackages = "ics.cs.support.model.dao")
public class HybridSecuritySampleApplication extends GlobalMethodSecurityConfiguration {

    public HybridSecuritySampleApplication() {
        System.out.println("...");
    }

    @Bean
    public ProductDAO getProductDAO() {
        // return new ProductDAOMock();
        return new ProductDAOImpl();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        // @formatter:off
        auth.inMemoryAuthentication().withUser("admin").password("p").roles("ADMIN", "USER").and().withUser("user")
                .password("p").roles("USER");
        // @formatter:on
    }

    @Bean
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return authenticationManager();
    }

    static {
        // Use a custom SecurityContextHolderStrategy
        SecurityContextHolder.setStrategyName(VaadinSessionSecurityContextHolderStrategy.class.getName());
    }

    @WebListener
    public static class SpringContextLoaderListener extends ContextLoaderListener {
    }

    @WebListener
    public static class MyRequestContextListener extends RequestContextListener {
    }

    @WebServlet (urlPatterns = "/*", name = "SecuredUIServlet", asyncSupported = true)
    @VaadinServletConfiguration (ui = SecuredUI.class, productionMode = false)
    public static class CustomerSupportUIServlet extends SpringVaadinServlet {
    }
}

// public static void main(String[] args) {
// SpringApplication.run(HybridSecuritySampleApplication.class, args);
// }
