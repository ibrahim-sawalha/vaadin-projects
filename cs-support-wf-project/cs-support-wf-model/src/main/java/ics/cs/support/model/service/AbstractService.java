package ics.cs.support.model.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

public class AbstractService {

    // @Autowired
    // DataSource dataSource;

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public JdbcTemplate getJdbcTemplate() {
        return jdbcTemplate;
    }

    public AbstractService() {

    }

    // public void setDataSource(DataSource dataSource) {
    // this.dataSource = dataSource;
    // this.jdbcTemplate = new JdbcTemplate(dataSource);
    // }

}
