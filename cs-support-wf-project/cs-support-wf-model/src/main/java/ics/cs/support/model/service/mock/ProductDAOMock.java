package ics.cs.support.model.service.mock;

import java.util.List;

import ics.cs.support.model.dao.ProductDAO;
import ics.cs.support.model.entity.Product;

// @Repository
public class ProductDAOMock implements ProductDAO {

    @Override
    public List<Product> getAllProducts() {
        return null;
    }
}
