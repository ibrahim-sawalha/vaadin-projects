package ics.cs.support.model.entity;

import java.math.BigDecimal;

public class Product {

    @Override
    public String toString() {
        return "Product [id=" + id + ", name=" + name + "]";
    }

    private BigDecimal id;
    private String name;

    public BigDecimal getId() {
        // test
        return id;
    }

    public void setId(BigDecimal id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
