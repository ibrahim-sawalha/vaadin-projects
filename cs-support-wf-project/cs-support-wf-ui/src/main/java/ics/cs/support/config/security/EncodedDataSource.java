package ics.cs.support.config.security;

import java.io.IOException;

import org.springframework.jdbc.datasource.DriverManagerDataSource;

public class EncodedDataSource extends DriverManagerDataSource {

    public EncodedDataSource() {
        super();
    }

    @Override
    public synchronized void setPassword(String encodedPassword) {
        super.setPassword(decode(encodedPassword));
    }

    private String decode(String password) {
        sun.misc.BASE64Decoder decoder = new sun.misc.BASE64Decoder();
        String decodedPassword = null;
        try {
            decodedPassword = new String(decoder.decodeBuffer(password));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return decodedPassword;
    }

    private static String encode(String str) {
        sun.misc.BASE64Encoder encoder = new sun.misc.BASE64Encoder();
        str = new String(encoder.encodeBuffer(str.getBytes()));
        return str;
    }

    public static void main(String[] args) {
        System.out.println(encode("banksys"));
    }

}
