package ics.cs.support.ui.view;

import java.util.logging.Logger;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;

import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.VerticalLayout;

import ics.cs.support.model.dao.ProductDAO;
import ics.cs.support.model.entity.Product;

@SpringView (name = "home")

public class HomeView extends VerticalLayout implements View {
    private static final long serialVersionUID = -5708809082571665980L;

    Logger logger = Logger.getLogger(HomeView.class.getName());

    @Autowired
    ProductDAO productDAO;

    public HomeView() {
        logger.info("Test Scope...");
        setMargin(true);
    }

    @PostConstruct
    public void init() {
        ComboBox productsComboBox = new ComboBox();
        ComboBox subProductsComboBox = new ComboBox();
        subProductsComboBox.setInputPrompt("Please select a Sub Product");
        subProductsComboBox.setEnabled(true);
        subProductsComboBox.setImmediate(true);

        BeanItemContainer<Product> productItemContainer = new BeanItemContainer<>(Product.class,
                productDAO.getAllProducts());
        productsComboBox.setContainerDataSource(productItemContainer);
        productsComboBox.setItemCaptionPropertyId("name");

        // configure & load content
        productsComboBox.addBlurListener(e -> {
            Product prod = (Product) productsComboBox.getValue();
            if (prod != null) {
                if (prod.getId().equals(1)) {
                    subProductsComboBox.setEnabled(true);
                } else {
                    subProductsComboBox.setEnabled(false);
                }
            }
        });

        // add to the layout
        addComponent(productsComboBox);

        // configure & load content
        subProductsComboBox.addBlurListener(e -> {
            logger.info((String) subProductsComboBox.getData());
            logger.info((String) subProductsComboBox.getValue());

        });
        subProductsComboBox.addItem("test-1");
        subProductsComboBox.addItem("test-2");
        subProductsComboBox.addItem("test-3");

        addComponent(new Button("Navigate to admin view", event -> {
            getUI().getNavigator().navigateTo("");
        }));

        // add to the layout
        addComponent(subProductsComboBox);

    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {
        // NOP
    }
}
