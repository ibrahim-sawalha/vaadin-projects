package ics.cs.support.config.security;

import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import com.vaadin.spring.access.ViewAccessControl;
import com.vaadin.spring.server.SpringVaadinServletService;
import com.vaadin.ui.UI;

/** This demonstrates how you can control access to views. */
@Component
public class SampleViewAccessControl implements ViewAccessControl {

    @Override
    public boolean isAccessGranted(UI ui, String beanName) {
        ApplicationContext applicationContext = ((SpringVaadinServletService) ui.getSession().getService())
                .getWebApplicationContext();
        System.out.println(applicationContext.containsBean(beanName));
        if (beanName.equals("adminView")) {
            return SecurityUtils.hasRole("ROLE_ADMIN");
        } else {
            return SecurityUtils.hasRole("ROLE_USER");
        }
    }
}
