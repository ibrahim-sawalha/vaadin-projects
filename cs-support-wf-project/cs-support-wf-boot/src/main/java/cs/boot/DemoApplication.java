package cs.boot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.method.configuration.GlobalMethodSecurityConfiguration;
import org.springframework.security.core.context.SecurityContextHolder;

import com.vaadin.spring.annotation.EnableVaadin;
import com.vaadin.spring.annotation.EnableVaadinNavigation;

import ics.cs.support.config.security.VaadinSessionSecurityContextHolderStrategy;

@SpringBootApplication
@EnableVaadin
@EnableVaadinNavigation
@EnableGlobalMethodSecurity (securedEnabled = true)

public class DemoApplication extends GlobalMethodSecurityConfiguration {

    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        // @formatter:off
        auth.inMemoryAuthentication().withUser("admin").password("p").roles("ADMIN", "USER").and().withUser("user")
                .password("p").roles("USER");
        // @formatter:on
    }

    @Bean
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return authenticationManager();
    }

    static {
        // Use a custom SecurityContextHolderStrategy
        SecurityContextHolder.setStrategyName(VaadinSessionSecurityContextHolderStrategy.class.getName());
    }

    // @WebListener
    // public static class SpringContextLoaderListener extends ContextLoaderListener {
    // }
    //
    // @WebListener
    // public static class MyRequestContextListener extends RequestContextListener {
    // }
    //
    // @WebServlet (urlPatterns = "/*", name = "SecuredUIServlet", asyncSupported = true)
    // @VaadinServletConfiguration (ui = SecuredUI.class, productionMode = false)
    // public static class CustomerSupportUIServlet extends SpringVaadinServlet {
    // }
}
